package com.admiral.financemanager;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.admiral.financemanager.control.Controller;
import com.admiral.financemanager.model.Modeler;
import com.admiral.financemanager.view.View;


public class MainActivity extends AppCompatActivity {

    private Modeler model;
    private View view;
    private Controller controller;


    private static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    private boolean initialized;

    protected void initialization()
    {
        this.model = new Modeler(this);
        this.view = new View(this);
        this.controller = new Controller(view, model);

        initialized = true;
    }

    public boolean checkAndRequestPermissions()
    {
        int permission = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS);
        if(permission == PackageManager.PERMISSION_GRANTED) {
            if(!initialized) initialization();
            return true;
        }

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_SMS}, REQUEST_ID_MULTIPLE_PERMISSIONS);
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checkAndRequestPermissions();

        if(initialized)
            controller.run();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == REQUEST_ID_MULTIPLE_PERMISSIONS)
            for(int i = 0; i < permissions.length; i++)
                switch (permissions[i]) {
                    case Manifest.permission.READ_SMS:
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                            Log.print("PERMISSION GRANTED");
                            if(!initialized) initialization();
                            return;
                        }
                        else if(grantResults[i] == PackageManager.PERMISSION_DENIED)
                        {
                            Log.print("PERMISSION DENIED");
                        }
                }

    }
}
