package com.admiral.financemanager.control;

import com.admiral.financemanager.model.Modeler;
import com.admiral.financemanager.model.UpdateProgress;
import com.admiral.financemanager.view.View;

import java.util.Calendar;

public class Controller implements ViewControl, ModelControl, MVC {

    private View view;
    private Modeler modeler;

    public Controller(View view, Modeler modeler)
    {
        this.view = view;
        this.modeler = modeler;
        this.modeler.setModelControl(this);
        this.view.setViewControl(this);
    }

    /** Стартовая инициализация */
    @Override
    public void run()
    {
        modeler.run();
        view.run();
    }

    /** ViewControl - интерфейс GUI */
    @Override
    public void readSMS() {
        modeler.updateSMS();
    }

    @Override
    public void addBankNumber(String number) {
        modeler.addNumber(number);
    }

    @Override
    public void choosePeriod(Calendar startPeriod, Calendar endPeriod) {
        view.updatePeriod(modeler.getData(startPeriod.getTimeInMillis(), endPeriod.getTimeInMillis()));
    }

    /** ModelControl - интерфейс модели данных*/
    @Override
    public void setUpdateProgress(UpdateProgress updateProgress) {
        view.setProgress(updateProgress);
    }
}
