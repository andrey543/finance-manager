package com.admiral.financemanager.control;

import java.util.Calendar;

public interface ViewControl {
    void readSMS();
    void addBankNumber(String number);
    void choosePeriod(Calendar startPeriod, Calendar endPeriod);

}
