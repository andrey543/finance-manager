package com.admiral.financemanager.control;

import com.admiral.financemanager.model.UpdateProgress;

public interface ModelControl {
    void setUpdateProgress(UpdateProgress updateProgress);
}
