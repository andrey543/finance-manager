package com.admiral.financemanager.model;

import com.admiral.financemanager.Key;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;

public class Data extends ArrayList<HashMap<String, Object>> {

    public Data()
    {

    }
    public ArrayList<Day> getDays()
    {
        ArrayList<Day> days = new ArrayList<>();
        Calendar nextTime = new GregorianCalendar();
        Day currentDay = null;

        for(int i = 0; i < this.size(); i++)
        {
            HashMap<String, Object> note = this.get(i);

            long ms = (long) note.get(Key.DATATIME);

            nextTime.setTimeInMillis(ms);
            Calendar nextDay = new GregorianCalendar(nextTime.get(
                    Calendar.YEAR),
                    nextTime.get(Calendar.MONTH),
                    nextTime.get(Calendar.DAY_OF_MONTH),0,0,0);

            // Начинаем с первого дня
            if(currentDay == null)
            {
                currentDay = new Day(nextDay, note);
                continue;
            }

            if( currentDay.getDate().equals(nextDay) )
            {
                currentDay.addNote(note);
            }
            else
            {
                days.add(currentDay);
                currentDay = new Day(nextDay, note);
            }
        }

        days.add(currentDay);

        return days;

    }
    public class Day
    {
        private ArrayList<HashMap<String, Object>> notes = new ArrayList<>();
        private Calendar date;
        private long total;
        private long lastBalance;

        public Day(Calendar date, HashMap<String, Object> note)
        {
            this.date = date;
            addNote(note);
        }

        public void addNote(HashMap<String, Object> note)
        {
            total += (long) note.get(Key.COST);
            lastBalance = (long) note.get(Key.BALANCE);
            notes.add(note);
        }

        public HashMap<String, Object> getNote(int i)
        {
            return notes.get(i);
        }
        public ArrayList<HashMap<String, Object>> getNotes()
        {
            return notes;
        }
        public int countNotes()
        {
            return notes.size();
        }

        public long getTotal()
        {
            return total;
        }

        public long getLastBalance() {
            return lastBalance;
        }

        public Calendar getDate()
        {
            return date;
        }

    }

}
