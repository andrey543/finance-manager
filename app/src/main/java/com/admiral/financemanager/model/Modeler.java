package com.admiral.financemanager.model;

import android.app.Activity;
import android.content.Context;

import com.admiral.financemanager.Log;
import com.admiral.financemanager.control.MVC;
import com.admiral.financemanager.control.ModelControl;

public class Modeler implements MVC {

    private ModelControl modelControl;

    private Activity activity;
    private FinanceBD db;
    private SMSReader reader;
    private SMSMonitor smsMonitor;
    private ManagerBankNumbers managerBankNumbers;
    private UpdateProgress progress;

    public Modeler(Activity activity)
    {
        this.activity = activity;
    }

    @Override
    public void run() {
        modelControl.setUpdateProgress(progress);
    }

    public void setModelControl(ModelControl modelControl)
    {
        this.modelControl = modelControl;
        this.db = new FinanceBD(activity);
        this.managerBankNumbers = new ManagerBankNumbers(activity.getPreferences(Context.MODE_PRIVATE));
        this.reader = new DBReader(activity.getContentResolver(), db);
        this.progress = new UpdateProgress();
    }

    public Data getData(long startPeriod, long endPeriod) {
        return this.db.updatePeriod(db.getReadableDatabase(), startPeriod, endPeriod);
    }

    public void addNumber(String number) {
        managerBankNumbers.add(number);
    }

    public void updateSMS() {
        if( !progress.isRunning() )
            reader.readAll(managerBankNumbers.getNumbers(), progress);
        else
            Log.print("Update process is already run");
    }
}
