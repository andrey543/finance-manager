package com.admiral.financemanager.model;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;

import com.admiral.financemanager.Log;
import com.admiral.financemanager.Key;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SMSReader {
    private static final String INBOX = "content://sms/inbox";

    private Cursor cursor;

    public SMSReader(ContentResolver contentResolver)
    {
        this.cursor = contentResolver.query(Uri.parse(INBOX), null,null,null,null);
    }

    public void readAll(final List<String> numbers, final UpdateProgress updateProgress)
    {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                if (cursor.moveToFirst()) {
                    List<ContentValues> content = new ArrayList<>();

                    updateProgress.setProcessName("Чтение смс...");
                    updateProgress.setMax(cursor.getCount());
                    updateProgress.setType(UpdateProgress.OutputType.PERC);
                    updateProgress.start();

                    /**
                     *  Анализируем ОДНО сообщение(СМС) */

                    do {
                        String msgData = "";
                        long datetime = 0;
                        updateProgress.increase();

                        // Определяем адрес, дату и текст сообщения
                        for (String name : cursor.getColumnNames()) {

                            // Читаем адрес(с какого номера пришла смс)
                            if (name.equals("address")) {
                                String address = cursor.getString(cursor.getColumnIndex(name));
                                boolean contains = false;
                                for (String number : numbers)
                                    if (number.equals(address)) {
                                        contains = true;
                                        break;
                                    }
                                if (!contains)
                                    break;

                                msgData += "\n" + "[NUMBER]:" + address + ": \n";
                            }

                            // Читаем дату
                            else if (name.equals("date")) {
                                datetime = Long.parseLong(cursor.getString(cursor.getColumnIndex(name)));
                                Date date = new Date(datetime);
                                msgData += "[DATE]: " + date.toString() + "\n";
                            }

                            // Читаем содержимое сообщения
                            else if (name.equals("body")) {
                                msgData += "[MESSAGE]: " + cursor.getString(cursor.getColumnIndex(name));
                            }
                        }
                        //Log.print(msgData);


                        /**
                         *  Определяем тип расхода(покупка, выдача наличными, перевод и т.д.) и значения*/

                        // Сначала смотрим не является ли этот тип не оплачиваемым(Недостаточно средств и т.п.)
                        boolean noCost = false;
                        for(String key : Key.Sberbank.keyNoCost)
                            if( !SMSParser.parse(msgData, key).isEmpty() )
                            {
                                noCost = true;
                                break;
                            }
                        if(noCost)
                            continue;


                        // Затем определяем тип расхода
                        String type = "";
                        for(String key : Key.Sberbank.keyCostWords)
                        {
                            type = SMSParser.parse(msgData, key);
                            if(!type.isEmpty())
                                break;
                        }
                        if(type.isEmpty())
                            continue;

                        // Выделяем из содержимого КЛЮЧЕВУЮ информацию
                        String cost = SMSParser.parse(SMSParser.parse(msgData, "(" + type + " [0-9]*)"), "[0-9]*");
                        String company = SMSParser.parse(SMSParser.parse(msgData,  type + " [0-9]*.*Баланс"), "[a-zA-Z]*(\\s)*");
                        String balance = SMSParser.parse(SMSParser.parse(msgData, "Баланс: [0-9]*"), "[0-9]*");

                        // Упаковываем ключевую инфу
                        ContentValues contentValues = new ContentValues();
                        contentValues.put(Key.TYPE, type);
                        contentValues.put(Key.COMPANY, company);
                        contentValues.put(Key.COST, cost);
                        contentValues.put(Key.BALANCE, balance);
                        contentValues.put(Key.DATATIME, datetime);
                        content.add(contentValues);

                    }
                    while (cursor.moveToNext());
                    // Специально выделил эту функцию для легкой замены функции чтения
                    // через базу данных или программным способом(через оперативку)
                    updateProgress.stop();
                    read(content, updateProgress);
                } else {
                    Log.print("EMPTY INBOX");
                }
            }
        });
        thread.start();

    }

    // Для переопределения в классах насладниках
    protected void read(List<ContentValues> content)
    {

    }
    protected void read(List<ContentValues> content, UpdateProgress progress)
    {

    }

    static class SMSParser
    {
        static String parse(String msgData, String regex)
        {
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(msgData);
            String msgParsing = "";
            while(matcher.find())
            {
                msgParsing += msgData.substring(matcher.start(), matcher.end());
            }

            return msgParsing;
        }
    }
}
