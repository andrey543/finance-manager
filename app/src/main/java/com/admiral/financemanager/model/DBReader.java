package com.admiral.financemanager.model;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

import com.admiral.financemanager.Key;

import java.util.Iterator;
import java.util.List;

public class DBReader extends SMSReader {
    private FinanceBD DBHelper;

    public DBReader(ContentResolver contentResolver, FinanceBD DBHelper) {
        super(contentResolver);
        this.DBHelper = DBHelper;
    }

    @Override
    protected void read(List<ContentValues> content, UpdateProgress progress)
    {
        progress.setProcessName("Обновление базы данных...");
        progress.setMax(content.size());
        progress.setType(UpdateProgress.OutputType.PERC);
        progress.start();

        SQLiteDatabase database = DBHelper.getWritableDatabase();
        DBHelper.clear(database);

        Iterator iterator = content.iterator();
        while (iterator.hasNext())
        {
            ContentValues contentValues = (ContentValues) iterator.next();
            if(!contentValues.get(Key.COST).equals("") && !contentValues.get(Key.BALANCE).equals(""))
                DBHelper.insert(contentValues, database);
            progress.increase();
        }
        progress.stop();

        DBHelper.read(database, "period", progress);
    }
    @Override
    protected void read(List<ContentValues> content)
    {
        SQLiteDatabase database = DBHelper.getWritableDatabase();
        DBHelper.clear(database);

        Iterator iterator = content.iterator();
        while (iterator.hasNext())
        {
            ContentValues contentValues = (ContentValues) iterator.next();
            if(!contentValues.get(Key.COST).equals("") && !contentValues.get(Key.BALANCE).equals(""))
                DBHelper.insert(contentValues, database);
        }

        DBHelper.read(database, "period");
    }
}
