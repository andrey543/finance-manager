package com.admiral.financemanager.model;

import android.content.SharedPreferences;

import com.admiral.financemanager.Log;

import java.util.ArrayList;
import java.util.List;

public class ManagerBankNumbers implements Added {
    private SharedPreferences sharedPreferences;

    public ManagerBankNumbers(SharedPreferences sharedPreferences)
    {
        this.sharedPreferences = sharedPreferences;
    }
    @Override
    public void add(String value) {
        addBankNumber(value);
    }
    private void addBankNumber(String bankNumber)
    {
        SharedPreferences.Editor editor = sharedPreferences.edit();

        int count = getCountBankNumbers();
        if(checkNumber(bankNumber)) {
            Log.print("[ADD]: " + bankNumber);
            editor.putString("NUMBER" + ++count, bankNumber);
            editor.putInt("COUNT_NUMBERS", count);
            editor.commit();

            for(int i = 1; i <= getCountBankNumbers(); i++)
            {
                Log.print("[" + i + "]: " + sharedPreferences.getString("NUMBER" + i,"NULL"));
            }
        }
        else
            Log.print("[NUMBER]: " + bankNumber + " already exists");

    }
    public List<String> getNumbers()
    {
        return updateNumbers();
    }
    private int getCountBankNumbers()
    {
        return sharedPreferences.getInt("COUNT_NUMBERS", 0);
    }
    private boolean checkNumber(String bankNumber)
    {
        for(int i = 1; i <= getCountBankNumbers(); i++)
            if(sharedPreferences.getString("NUMBER" + i, "NULL").equals(bankNumber))
                return false;
        return true;
    }
    private List<String> updateNumbers()
    {
        List<String> numbers = new ArrayList<>();
        for(int i = 1; i <= getCountBankNumbers(); i++) {
            String number = sharedPreferences.getString("NUMBER" + i, "NULL");
            if(number != null && !number.equals("NULL"))
                numbers.add(number);
        }

        return numbers;
    }
}
