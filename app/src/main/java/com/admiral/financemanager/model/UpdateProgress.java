package com.admiral.financemanager.model;

public class UpdateProgress {

    private UpdateProgressListener listener;
    private float max = 0;
    private float min = 0;
    private float k = 1.0f;
    private float process = 0;
    private OutputType type = OutputType.ORIGINAL;
    private boolean isRunning = false;
    private String processName = "Unknown process";

    public UpdateProgress() {

    }

    public void setProcessName(String processName) {
        this.processName = processName;
    }

    public enum OutputType
    {
        ORIGINAL,
        PERC
    }
    public void setUpdateProgressListener(UpdateProgressListener listener)
    {
        this.listener = listener;
    }

    public void start() {
        isRunning = true;
        process = 0;
        set(getProcess());
        if(listener != null)
        {
            listener.startProcess(isRunning, processName);
        }
    }

    public void stop() {
        isRunning = false;
        if(listener != null)
        {
            listener.startProcess(isRunning, processName);
        }
    }

    public boolean isRunning() {
        return isRunning;
    }

    public int getProcess() {
        switch (type)
        {
            case ORIGINAL:
                return (int) this.process;

            case PERC:
                return updatePerc();
        }

        return 1;
    }

    public void setType(OutputType type)
    {
        this.type = type;
    }

    public void set(int process) {
        this.process = process;
        if(listener != null)
        {
            listener.updateProcess(getProcess());
        }
    }
    public void add(int part)
    {
        this.process += part;
        if(listener != null)
        {
            listener.updateProcess(getProcess());
        }
    }
    public void increase()
    {
        this.process++;
        if(listener != null)
        {
            listener.updateProcess(getProcess());
        }
    }
    public void setMax(int max)
    {
        this.max = max;
        updateK();
    }
    public void setMin(int min)
    {
        this.min = min;
        updateK();
    }
    private void updateK()
    {
        k = Math.abs(min - max) == 0 ? 1 : Math.abs(min - max);
    }
    private int updatePerc()
    {
       return Math.round((this.process / k) * 100);
    }

    public interface UpdateProgressListener
    {
        void startProcess(boolean cmd, String processName);
        void updateProcess(int value);
    }

}
