package com.admiral.financemanager.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.admiral.financemanager.Key;
import com.admiral.financemanager.view.MainPanel;
import com.admiral.financemanager.Log;

import java.util.HashMap;

public class FinanceBD extends SQLiteOpenHelper {

    private static final String MAIN_TABLE = "finance";
    private static final String PERIOD_TABLE = "period";

    private MainPanel panel;

    public FinanceBD(Context context) {
        super(context, "FinanceBD", null, 3);
        this.panel = panel;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.print("OnCreate data base...");
        db.execSQL("create table finance ("
                + "id integer primary key autoincrement,"
                + "type text,"
                + "company text,"
                + "cost integer,"
                + "balance integer,"
                + "datetime integer);");
        db.execSQL("create table period ("
                + "id integer primary key autoincrement,"
                + "type text,"
                + "company text,"
                + "cost integer,"
                + "balance integer,"
                + "datetime integer);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("create table finance ("
                + "id integer primary key autoincrement,"
                + "type text,"
                + "company text,"
                + "cost integer,"
                + "balance integer,"
                + "datetime integer);");
        db.execSQL("create table period ("
                + "id integer primary key autoincrement,"
                + "type text,"
                + "company text,"
                + "cost integer,"
                + "balance integer,"
                + "datetime integer);");
    }

    public void insert(ContentValues contentValues, SQLiteDatabase database) {
        database.insert(MAIN_TABLE, null, contentValues);
    }

    public void clear(SQLiteDatabase database) {
        database.execSQL("delete from finance");
        database.execSQL("update sqlite_sequence set seq=0");
    }

    public boolean isUpdated(SQLiteDatabase database)
    {
        Cursor cursor = database.query(MAIN_TABLE,null,null,null,null,null,null);

        return cursor.moveToFirst();
    }
    public Data updatePeriod(SQLiteDatabase database, long startPeriod, long endPeriod)
    {
        database.execSQL("delete from period");
        database.execSQL("update sqlite_sequence set seq=0");
        database.execSQL("insert into period select * from finance where datetime between " + startPeriod + " and " + endPeriod + ";");

        return read(database, PERIOD_TABLE);
    }
    public Data read(SQLiteDatabase database, String table)
    {
        Cursor cursor = database.query(table,null,null,null,null,null,null);
        Data data = new Data();

        Log.print("---------- DATABASE ----------");
        Log.print("---------- " + table + " ----------");
        if(cursor.moveToFirst()) {
            do {

                String str = "";
                HashMap<String, Object> note = new HashMap<>();

                for(String name : cursor.getColumnNames())
                {
                    if(name.equals(Key.TYPE))
                    {
                        note.put(Key.TYPE, cursor.getString(cursor.getColumnIndex(name)));
                    }
                    if(name.equals(Key.COST))
                    {
                        note.put(Key.COST, Long.parseLong(cursor.getString(cursor.getColumnIndex(name))));
                    }
                    else if(name.equals(Key.COMPANY))
                    {
                        note.put(Key.COMPANY, cursor.getString(cursor.getColumnIndex(name)));
                    }
                    else if(name.equals(Key.BALANCE))
                    {
                        note.put(Key.BALANCE, Long.parseLong(cursor.getString(cursor.getColumnIndex(name))));
                    }
                    else if(name.equals(Key.DATATIME))
                    {
                        note.put(Key.DATATIME, Long.parseLong(cursor.getString(cursor.getColumnIndex(name))));
                    }

                    str += "[" + name + "]: " + cursor.getString(cursor.getColumnIndex(name)) + " ";
                }
                data.add(note);

                Log.print(str);
            }
            while (cursor.moveToNext());
        }
        else
        {
            Log.print("Database Finance is EMPTY");
        }

        return data;

    }

    public Data read(SQLiteDatabase database, String table, UpdateProgress progress)
    {

        Cursor cursor = database.query(table,null,null,null,null,null,null);
        Data data = new Data();

        progress.setProcessName("Чтение базы данных...");
        progress.setMax(cursor.getCount());
        progress.setType(UpdateProgress.OutputType.PERC);
        progress.start();

        Log.print("---------- DATABASE ----------");
        Log.print("---------- " + table + " ----------");
        if(cursor.moveToFirst()) {
            do {

                String str = "";
                HashMap<String, Object> note = new HashMap<>();

                for(String name : cursor.getColumnNames())
                {
                    if(name.equals(Key.TYPE))
                    {
                        note.put(Key.TYPE, cursor.getString(cursor.getColumnIndex(name)));
                    }
                    if(name.equals(Key.COST))
                    {
                        note.put(Key.COST, Long.parseLong(cursor.getString(cursor.getColumnIndex(name))));
                    }
                    else if(name.equals(Key.COMPANY))
                    {
                        note.put(Key.COMPANY, cursor.getString(cursor.getColumnIndex(name)));
                    }
                    else if(name.equals(Key.BALANCE))
                    {
                        note.put(Key.BALANCE, Long.parseLong(cursor.getString(cursor.getColumnIndex(name))));
                    }
                    else if(name.equals(Key.DATATIME))
                    {
                        note.put(Key.DATATIME, Long.parseLong(cursor.getString(cursor.getColumnIndex(name))));
                    }

                    str += "[" + name + "]: " + cursor.getString(cursor.getColumnIndex(name)) + " ";
                }
                data.add(note);

                Log.print(str);
            }
            while (cursor.moveToNext());
        }
        else
        {
            Log.print("Database Finance is EMPTY");
        }

        progress.stop();

        return data;

    }
}
