package com.admiral.financemanager.view;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.admiral.financemanager.Log;
import com.admiral.financemanager.R;
import com.admiral.financemanager.Key;
import com.admiral.financemanager.model.Data;
import com.admiral.financemanager.view.fragments.DayCard;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

public class MainPanel {

    private LinearLayout layout;
    private Activity activity;
    private FragmentTransaction fragmentTransaction;

    public MainPanel(Activity activity)
    {
        this.layout = activity.findViewById(R.id.mainPanelLayout);
        this.activity = activity;
    }

    public void update(Data data)
    {
        layout.removeAllViews();

        fragmentTransaction = activity.getFragmentManager().beginTransaction();

        if( data.size() == 0 )
        {

            Log.print("Database Finance is EMPTY");
            addText("Database Finance is EMPTY");
        }
        else
        {
            for (Data.Day day : data.getDays())
            {
                DayCard card = new DayCard();
                fragmentTransaction.add(R.id.mainPanelLayout, card);

                card.setTextDate(formatDate("dd-MM-YYYY", day.getDate().getTimeInMillis()));
                //addText(formatDate("dd-MM-YYYY", day.getDate().getTimeInMillis()));
                for(HashMap<String, Object> note : day.getNotes())
                {
                    long time      = (long) note.get(Key.DATATIME);
                    long cost      = (long) note.get(Key.COST);
                    String type    = (String) note.get(Key.TYPE);
                    String company = (String) note.get(Key.COMPANY);

                    card.addTextNotes("[" + formatDate("HH:mm:ss", time) + "] " + company + " " + type + " сумма: " + cost + "р.");

                    //addText("   [" + formatDate("hh:mm:ss", time) + "] " + company + " " + type + " сумма: " + cost);
                }
                card.setTextTotal("Потрачено за день: " + day.getTotal() + " р.");
                card.setTextBalance("Остаток: " + day.getLastBalance() + " р.");
                //addText("   ПОТРАЧЕНО ЗА ДЕНЬ: " + day.getTotal() + "              ОСТАТОК: " + day.getLastBalance());
            }

        }
        fragmentTransaction.commit();
    }
    private void addText(String text)
    {
        TextView textView = new TextView(activity);
        textView.setText(text);
        layout.addView(textView,layout.getLayoutParams());
    }
    private String formatDate( String format, long ms )
    {
        return new SimpleDateFormat(format).format(new Date(ms));
    }
}
