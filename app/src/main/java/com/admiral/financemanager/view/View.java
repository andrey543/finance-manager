package com.admiral.financemanager.view;

import android.widget.Button;
import android.widget.Toast;

import com.admiral.financemanager.control.MVC;
import com.admiral.financemanager.control.ViewControl;
import com.admiral.financemanager.Log;
import com.admiral.financemanager.MainActivity;
import com.admiral.financemanager.R;
import com.admiral.financemanager.model.Data;
import com.admiral.financemanager.model.UpdateProgress;

public class View implements MVC
{
    private MainActivity activity;
    private ViewControl viewControl;
    private MainPanel mainPanel;
    private PeriodSpinner periodSpinner;
    private Progress progress;
    private Buttons buttons;

    public View(MainActivity activity)
    {
        activity.setContentView(R.layout.activity_main);
        this.activity = activity;
    }

    public void setViewControl(ViewControl viewControl) {
        this.viewControl = viewControl;
        this.progress = new Progress(activity);
        this.mainPanel = new MainPanel(activity);
        this.buttons = new Buttons();
        this.periodSpinner = new PeriodSpinner(activity, viewControl);

    }

    @Override
    public void run() {
        periodSpinner.setSelection(0);
        viewControl.readSMS();
    }

    // Интерфейс пользователя
    private class Buttons
    {
        Button readSms = activity.findViewById(R.id.readButton);
        Button addNumber = activity.findViewById(R.id.chooseNumber);

        Buttons()
        {
            readSms.setOnClickListener(new android.view.View.OnClickListener() {
                @Override
                public void onClick(android.view.View v) {
                    if(activity.checkAndRequestPermissions())
                        viewControl.readSMS();
                    else {
                        Log.print("SMS READ PERMISSION DENIED");
                        Toast.makeText(activity, "SMS READ PERMISSION DENIED", Toast.LENGTH_LONG).show();
                    }
                }
            });
            addNumber.setOnClickListener(new android.view.View.OnClickListener() {
                @Override
                public void onClick(android.view.View v) {
                    new InputDialog(activity, viewControl);
                }
            });
        }
    }
    public void updatePeriod(Data data)
    {
        mainPanel.update(data);
    }
    public void setProgress(UpdateProgress updateProgress)
    {
        progress.setProgress(updateProgress);
    }
}
