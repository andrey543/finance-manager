package com.admiral.financemanager.view.fragments;

import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.admiral.financemanager.R;

import java.util.ArrayList;
import java.util.List;

public class DayCard extends Fragment {

    private View root;
    private String date;
    private String total;
    private String balance;
    private List<String> tvNotes = new ArrayList<>();


    public DayCard()
    {
    }

    public void setTextDate(String text) {

        this.date = text;
    }

    public void setTextTotal(String text) {
        this.total = text;
    }

    public void setTextBalance(String text) {
        this.balance = text;
    }

    public void addTextNotes(String text) {
       tvNotes.add(text);
    }
    private void updateNotes()
    {
        LinearLayout layout = root.findViewById(R.id.notePanel);
        for(String note : tvNotes) {
            TextView textView = new TextView(this.getActivity());
            textView.setTextColor(Color.BLACK);
            textView.setTextSize(16);
            textView.setText(note);
            layout.addView(textView);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        this.root = inflater.inflate(R.layout.fragment_day, container,false);

        TextView tvDate = root.findViewById(R.id.tvDate);
        TextView tvTotal = root.findViewById(R.id.tvTotal);
        TextView tvBalance = root.findViewById(R.id.tvBalance);

        tvDate.setText(date);
        tvTotal.setText(total);
        tvBalance.setText(balance);

        updateNotes();

        return root;
    }

}
