package com.admiral.financemanager.view;

import android.app.Activity;
import android.os.Handler;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.admiral.financemanager.R;
import com.admiral.financemanager.model.UpdateProgress;



public class Progress implements UpdateProgress.UpdateProgressListener {

    private Activity activity;
    private ProgressBar bar;
    private TextView textView;
    private Handler handler;
    private boolean isRun;

    public Progress(Activity activity) {
        this.activity = activity;
        this.bar = activity.findViewById(R.id.progressBarUpdate);
        this.textView = activity.findViewById(R.id.textViewProgress);
        this.handler = new Handler();

        textView.setVisibility(View.INVISIBLE);
        bar.setVisibility(View.INVISIBLE);
    }


    public void setProgress(UpdateProgress updateProgress)
    {
       updateProgress.setUpdateProgressListener(this);
    }

    @Override
    public void startProcess(boolean cmd, final String processName) {

        isRun = cmd;
        if(isRun)
        {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    textView.setText(processName);
                    onVisible();
                }
            });

        }
        else {

            handler.post(new Runnable() {
                @Override
                public void run() {
                    offVisible();
                }
            });

        }

    }

    @Override
    public void updateProcess(int value) {
        bar.setProgress(value);
    }

    private void onVisible()
    {
        textView.setVisibility(View.VISIBLE);
        bar.setVisibility(View.VISIBLE);
    }
    private void offVisible()
    {
        textView.setVisibility(View.INVISIBLE);
        bar.setVisibility(View.INVISIBLE);
    }

}
