package com.admiral.financemanager.view;

import android.app.Activity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.admiral.financemanager.control.ViewControl;
import com.admiral.financemanager.Log;
import com.admiral.financemanager.R;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class PeriodSpinner {
    private final Spinner spinner;
    private final ViewControl viewControl;

    public PeriodSpinner(Activity activity, ViewControl viewControl) {
        // Выплывающий список 'Периоды'
        String periods[] = {"day", "week", "two_week", "month", "year"};
        ArrayAdapter<String> adapter = new ArrayAdapter<>(activity, android.R.layout.simple_spinner_item, periods);
        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);

        this.viewControl = viewControl;

        spinner = activity.findViewById(R.id.spinnerPeriods);
        spinner.setAdapter(adapter);
        spinner.setPrompt("Choose period");
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                setSelection(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
    public void setSelection(int position)
    {
        spinner.setSelection(position);
        String period = (String) spinner.getSelectedItem();

        if(period.equals("day"))
        {
            Calendar currentDay = new GregorianCalendar();
            Calendar startOfDay = new GregorianCalendar(
                    currentDay.get(Calendar.YEAR),
                    currentDay.get(Calendar.MONTH),
                    currentDay.get(Calendar.DAY_OF_MONTH));

            Log.print(startOfDay.getTime().toString());
            Log.print(currentDay.getTime().toString());

            viewControl.choosePeriod(startOfDay, currentDay);

        }
        else if(period.equals("week"))
        {
            Calendar currentDay = new GregorianCalendar();
            // Перевод на "наш" календарь т.к. по дефолту 1й день это воскресенье,
            // а суббота 7й
            int dayOfWeek = currentDay.get(Calendar.DAY_OF_WEEK) - 1;
            dayOfWeek = dayOfWeek == 0 ? 7 : dayOfWeek;

            getLastDays(dayOfWeek);
        }
        else if(period.equals("two_week"))
        {
            Calendar currentDay = new GregorianCalendar();
            // Перевод на "наш" календарь т.к. по дефолту 1й день это воскресенье,
            // а суббота 7й
            int dayOfWeek = currentDay.get(Calendar.DAY_OF_WEEK) - 1;
            dayOfWeek = dayOfWeek == 0 ? 7 : dayOfWeek;

            getLastDays(dayOfWeek + 7);
        }
        else if(period.equals("month"))
        {
            Calendar currentDay= new GregorianCalendar();
            Calendar firstDayOfMonth = new GregorianCalendar(currentDay.get(Calendar.YEAR), currentDay.get(Calendar.MONTH),1);

            Log.print(firstDayOfMonth.getTime().toString());
            Log.print(currentDay.getTime().toString());

            viewControl.choosePeriod(firstDayOfMonth, currentDay);

        }
        else if(period.equals("year"))
        {
            Calendar currentDay= new GregorianCalendar();
            Calendar firstDayOfMonth = new GregorianCalendar(currentDay.get(Calendar.YEAR), 0,1);

            Log.print(firstDayOfMonth.getTime().toString());
            Log.print(currentDay.getTime().toString());

            viewControl.choosePeriod(firstDayOfMonth, currentDay);
        }
    }
    private void getLastDays(int days)
    {
        Calendar calendar = new GregorianCalendar();
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);

        Calendar startDay = new GregorianCalendar(year,month,day);
        long lastDays = (long)((days - 1) * 86400);
        long lastWeek = startDay.getTimeInMillis() - lastDays * 1000;
        Calendar startPrevWeekDay = new GregorianCalendar();
        startPrevWeekDay.setTimeInMillis(lastWeek);
        Calendar currentDay = new GregorianCalendar(year,month,day,23,59,59);
        Date newDate1 = startPrevWeekDay.getTime();
        Date newDate2 = currentDay.getTime();

        Log.print(newDate1.toString());
        Log.print(newDate2.toString());

        viewControl.choosePeriod(startPrevWeekDay, currentDay);
    }
}
