package com.admiral.financemanager.view;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.EditText;

import com.admiral.financemanager.control.ViewControl;
import com.admiral.financemanager.Log;

public class InputDialog {

    private AlertDialog.Builder alert;

    public InputDialog(Context context, final ViewControl viewControl) {
        alert = new AlertDialog.Builder(context);

        alert.setTitle("Bank number");
        alert.setMessage("Choose bank number");

        final EditText input = new EditText(context);
        alert.setView(input);

        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                viewControl.addBankNumber(input.getText().toString());
            }
        });

        alert.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Log.print("CANCEL");
            }
        });
        alert.show();
    }
}
