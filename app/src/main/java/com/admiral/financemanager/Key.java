package com.admiral.financemanager;

public class Key {

    // Включить базу данных для хранения СМС
    public static boolean onDataBase = true;

    public static final String COMPANY = "company";
    public static final String COST = "cost";
    public static final String BALANCE = "balance";
    public static final String DATATIME = "datetime";
    public static final String TYPE = "type";

    static public class Sberbank
    {
        public static String[] keyCostWords = {
                "(п|П)окупка",
                "(п|П)еревод",
                "(в|В)ыдача"
                            };
        public static String[] keyNoCost = {"(н|Н)едостаточно средств"};
    }

}
